package com.rhytify.lambda;

public class InstanceRequest {

    private String instanceId;

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public InstanceRequest(String instanceId) {
        this.instanceId = instanceId;
    }

    public InstanceRequest() {
    }
}
